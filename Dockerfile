FROM openjdk:11

COPY target/Springapp.jar Springapp.jar

ENTRYPOINT ["java", "-jar", "/Springapp.jar"]
