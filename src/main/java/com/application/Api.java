package com.application;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class Api {

    @GetMapping
    public List<String> getList(){
        return new ArrayList<>(List.of("Kien","Hoa","Hieu","Quang"));
    }

    @GetMapping(value = "/user")
    public String getUser(){
        return new StringBuilder("It's working fine!").toString();
    }
}
